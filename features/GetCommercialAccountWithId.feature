Feature: Get Commercial Account With Id
  Gets the commercial account with the provided id

  Scenario: Success
    Given I provide the accountId of an existing commercial account in the account-service
    And provide a valid accessToken
    When I execute getAccountWithId
    Then the commercial account is returned