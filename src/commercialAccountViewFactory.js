import CommercialAccountView from './commercialAccountView';
import {PostalAddressFactory} from 'postal-object-model';

export default class CommercialAccountViewFactory {

    static construct(data):CommercialAccountView {

        const name = data.name;

        const address = PostalAddressFactory.construct(data.address);

        const id = data.id;

        return new CommercialAccountView(
            name,
            address,
            id
        );

    }

}
