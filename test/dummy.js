import {PostalAddress} from 'postal-object-model';

const dummy = {
    accountName: 'accountName',
    firstName: 'firstName',
    lastName: 'lastName',
    phoneNumber: '0000000000',
    streetAddress: 'street',
    city: 'city',
    iso31662Code: 'WA',
    postalCode: 'postalCode',
    iso31661Alpha2Code: 'US',
    customerSegmentId: 1,
    customerBrandId: 1,
    accountId: '000000000000000000',
    sapAccountNumber: 'sapAccountNo',
    sapVendorNumber: '0000000000',
    userId: 'email@test.com',
    url: 'https://dummy-url.com'
};

dummy.postalAddress =
    new PostalAddress(
        dummy.streetAddress,
        dummy.city,
        dummy.iso31662Code,
        dummy.postalCode,
        dummy.iso31661Alpha2Code
    );

/**
 * dummy objects (see: http://xunitpatterns.com/Dummy%20Object.html)
 */
export default dummy;