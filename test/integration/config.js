import AccountServiceSdkConfig from '../../src/accountServiceSdkConfig';

export default {
    accountServiceSdkConfig: new AccountServiceSdkConfig(
        'https://api-dev.precorconnect.com'
    ),
    identityServiceJwtSigningKey: 'nbho9k9vcv8r48xGQs4woyN8BJ6q9X1efj295KXfS9A9yHJSRm0oU21j3ickrScQ',
    existingPartnerAccountAssociation: {
		partnerAccountId: '001K000001H289FIAR', 
		partialName: 'accountName'
    },
    existingCustomerSegmentId: 1,
    existingCustomerBrandId: 1
}