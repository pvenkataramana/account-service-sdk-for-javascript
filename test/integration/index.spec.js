import AccountServiceSdk,
{
    AddCommercialAccountReq,
    SearchCommercialAccountsAssociatedWithPartnerReq,
} from '../../src/index';
import {PostalAddress} from 'postal-object-model';
import CommercialAccountView from '../../src/commercialAccountView';
import config from './config';
import factory from './factory';
import dummy from '../dummy';

/*
 test methods
 */
describe('Index module', () => {

    describe('default export', () => {
        it('should be AccountServiceSdk constructor', () => {

            /*
             act
             */
            const objectUnderTest =
                new AccountServiceSdk(config.accountServiceSdkConfig);

            /*
             assert
             */
            expect(objectUnderTest).toEqual(jasmine.any(AccountServiceSdk));

        });
    });

    describe('instance of default export', () => {

        describe('addCommercialAccount method', () => {
            it('should return accountId', (done) => {

                    /*
                     arrange
                     */
                    const objectUnderTest =
                        new AccountServiceSdk(config.accountServiceSdkConfig);

                    /*
                     act
                     */
                    const accountIdPromise =
                        objectUnderTest.addCommercialAccount(
                            factory.constructValidAddCommercialAccountRequest(),
                            factory.constructValidPartnerRepOAuth2AccessToken()
                        );

                    /*
                     assert
                     */

                    accountIdPromise
                        .then((accountId) => {
                            expect(accountId).toBeTruthy();
                            done();
                        })
                        .catch(error=> done.fail(JSON.stringify(error)));

                },
                20000);
        });
        describe('getCommercialAccountWithId method', () => {
            it('should return expected CommercialAccountView', (done) => {
                    /*
                     arrange
                     */
                    let expectedCommercialAccountView;
                    let addCommercialAccountRequest =
                        factory.constructValidAddCommercialAccountRequest();

                    const objectUnderTest =
                        new AccountServiceSdk(config.accountServiceSdkConfig);

                    // seed a new account so we can test the retrieval of it
                    const expectedAccountIdPromise =
                        objectUnderTest.addCommercialAccount(
                            addCommercialAccountRequest,
                            factory.constructValidPartnerRepOAuth2AccessToken()
                            )
                            .then(
                                (expectedAccountId) => {

                                    // construct expected account view
                                    expectedCommercialAccountView =
                                        new CommercialAccountView(
                                            addCommercialAccountRequest.name,
                                            addCommercialAccountRequest.address,
                                            expectedAccountId
                                        );

                                    return expectedAccountId;
                                }
                            );

                    /*
                     act
                     */
                    const actualCommercialAccountViewPromise =
                        expectedAccountIdPromise
                            .then((expectedAccountId)=> {

                                // get account
                                return objectUnderTest.getCommercialAccountWithId(
                                    expectedAccountId,
                                    factory.constructValidPartnerRepOAuth2AccessToken()
                                )

                            });


                    /*
                     assert
                     */
                    actualCommercialAccountViewPromise
                        .then((actualCommercialAccountView) => {
                            expect(actualCommercialAccountView).toEqual(expectedCommercialAccountView);
                            done();
                        })
                        .catch(error=> done.fail(JSON.stringify(error)));

                },
                20000);
        });
        describe('searchCommercialAccountsAssociatedWithPartner', () => {
            it('should return more than 0 results', (done) => {

                    /*
                     arrange
                     */
                    const objectUnderTest =
                        new AccountServiceSdk(config.accountServiceSdkConfig);

                    const searchCommercialAccountsAssociatedWithPartnerReq =
                        new SearchCommercialAccountsAssociatedWithPartnerReq(
                            config.existingPartnerAccountAssociation.partialName,
                            config.existingPartnerAccountAssociation.partnerAccountId
                        );

                    /*
                     act
                     */
                    const accountSynopsesPromise =
                        objectUnderTest.searchCommercialAccountsAssociatedWithPartner(
                            searchCommercialAccountsAssociatedWithPartnerReq,
                            factory.constructValidPartnerRepOAuth2AccessToken()
                        );

                    /*
                     assert
                     */

                    accountSynopsesPromise
                        .then((accountSynopses) => {
                            expect(accountSynopses.length).toBeGreaterThan(0);
                            done();
                        })
                        .catch(error=> done.fail(JSON.stringify(error)));

                },
                20000);
        });

    });
});